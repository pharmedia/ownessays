RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :account
  end
  config.current_user_method(&:current_account)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0
  config.audit_with :paper_trail, 'Account', 'PaperTrail::Version'

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.label_methods << 'tag'

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    history_index
    history_show
    nestable
  end

  config.model Block do
    field :tag
    field :content, :ck_editor

    nestable_tree({ position_field: :position, max_depth: 3 })
  end

  config.model Post do
      field :title
      field :subtitle
      field :slug
      field :author
      field :category

      field :content, :ck_editor

      field :photo
  end

  config.model Coupon do
    field :code
    field :percent_value
    field :fixed_value
    field :is_active
    field :valid_from
    field :valid_to
  end

  config.model Page do
    field :title
    field :slug
    field :subtitle
    field :content, :ck_editor

    field :base_color

    field :blocks do
      orderable true
    end

    field :parent_id, :enum do
      enum_method do
        :parent_enum
      end
    end

    nestable_tree({ position_field: :position, max_depth: 3 })
  end
end
