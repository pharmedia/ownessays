Rails.application.routes.draw do

  root 'main#index'

  get 'privacy' => 'main#privacy'
  get 'terms-and-conditions' => 'main#terms', as: "terms"
  get 'pay-if-you-like' => "main#pay_if_you_like"

  post 'order' => 'order#initiate'
  get 'order' => 'order#index'

  post 'upload' => 'order#upload'

  get 'blog' => 'blog#index'
  match 'blog/:slug' => 'blog#show', as: 'show_post', via: :get

  post 'contact' => 'contact#index'

  devise_for :accounts
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/backoffice', as: 'rails_admin'

end
