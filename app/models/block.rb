class Block < ActiveRecord::Base
  # attr_accessible :page_ids
  has_paper_trail
  has_ancestry

  belongs_to :page

  validates :tag, :content, presence: true
end
