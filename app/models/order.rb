class Order < ActiveRecord::Base
  has_many :uploads

  validates :name, :email, :phone, :subject, :topic, :academic_level, :type_of_paper,
            :sources, :pages, :powerpoint_slides, :category_of_writer,
            :instructions, :deadline, :paper_format, presence: true

  validates :pages, :powerpoint_slides, :sources, numericality: true

  has_paper_trail

  before_save :trim_instructions

  private
  def trim_instructions
  	instructions.gsub!(/[^0-9A-Za-z ]/, '')
  end
end