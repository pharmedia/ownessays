class Post < ActiveRecord::Base
  has_paper_trail

  validates :title, :subtitle, :slug, :author, :category, :content, presence: true

  has_attached_file :photo, styles: { original: "1024x1024>", thumb: "256x256>" },
                    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/

  def to_param
    slug || title.downcase.gsub(" ", "-").gsub(/<[^>]*>/ui,'')
  end
end
