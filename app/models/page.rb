class Page < ActiveRecord::Base
  has_ancestry
  has_paper_trail

  has_many :blocks, dependent: :destroy

  validates :title, :slug, :content, presence: true

  def parent_enum
    Page.where.not(id: id).map { |c| [ c.title, c.id ] }
  end
end
