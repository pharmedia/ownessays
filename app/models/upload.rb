class Upload < ActiveRecord::Base
  belongs_to :order

  has_attached_file :document
  validates_attachment_content_type :document,
                       content_type: ["application/vnd.ms-office",
                                      "application/msword",
                                      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                      "application/pdf",
                                      "image/jpeg",
									  "image/png"]

end
