class Coupon < ActiveRecord::Base
  validates :code, presence: true

  has_paper_trail
end
