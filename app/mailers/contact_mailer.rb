class ContactMailer < ApplicationMailer
  def contact_email(message)
    receiver = 'ownessays.team@gmail.com'
    @message = message

    mail(to: receiver, subject: 'OwnEssays: Contact Form',
         bcc: ['enzo.nieri@gmail.com'])
  end
end
