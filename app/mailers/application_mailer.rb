class ApplicationMailer < ActionMailer::Base
  default from: 'OwnEssays <orders@ownessays.com>'
  layout 'mailer'
end
