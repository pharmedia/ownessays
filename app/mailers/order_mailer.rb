class OrderMailer < ApplicationMailer
  default from: 'OwnEssay Orders <orders@ownessays.com>'

  def confirmation_email(for_order)
    receiver = 'ownessays.team@gmail.com' #'rlysyuk@gmail.com'
    @order_hash = for_order

    assign_attachments(for_order)

    mail(to: receiver, subject: 'OwnEssays: Completed',
         bcc: ['enzo.nieri@gmail.com'])
  end

  def failed_attempt_email(for_order)
    receiver = 'ownessays.team@gmail.com'
    @order_hash = for_order

    assign_attachments(for_order)

    mail(to: receiver, subject: 'OwnEssays: Failed',
         bcc: ['enzo.nieri@gmail.com'])
  end

  def assign_attachments(for_order)
      for_order.uploads.each do |upload|
        attachments[upload.document_file_name] = File.read(upload.document.path) if upload.try(:document)
      end if for_order.uploads.any?
  end
end