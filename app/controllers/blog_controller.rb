class BlogController < ApplicationController
  def index
    @posts = Post.order(updated_at: :desc).page params[:page]
    @page_title = 'OwnEssays.com | Blog'
    @page_title += " - Page #{params['page']}" if params[:page].present?
  end

  def show
    @post = Post.find_by_slug(params[:slug])
    @page_title = @post.title if @post
    @non_ajax = !request.xhr?
    render :layout => @non_ajax
  end
end
