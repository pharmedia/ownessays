class ContactController < ApplicationController
  def index
    fields = %w(name email message)
    message = Message.create(params.permit(fields))
    message.save

    ContactMailer.contact_email(message).deliver_now!

    render text: "SEND"
  end
end
