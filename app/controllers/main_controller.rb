class MainController < ApplicationController
  def index
    @posts = Post.order(updated_at: :desc).limit(4)

  end

  def pay_if_you_like
    @page_title = 'Pay if you like | OwnEssays.com'
  end

  def privacy
    @page_title = 'Privacy Policy | OwnEssays.com'
  end

  def terms
    @page_title = 'Terms and Conditions | OwnEssays.com'
  end
end
