class OrderController < ApplicationController
  def index
  end

  def initiate

    order = Order.new do |o|
      o.name = order_params["name"]
      o.phone = order_params["phone"]
      o.email = order_params["email"]

      o.coupon = order_params["coupon"]

      o.subject = order_params["subject"]
      o.topic = order_params["topic"]
      o.academic_level = order_params["academicLevel"]
      o.type_of_paper = order_params["typeOfPaper"]
      o.sources = order_params["quantity"]
      o.pages = order_params["pageQuantity"]
      o.paper_format = order_params["paperFormat"]
      o.powerpoint_slides = order_params["slideQuantity"]
      o.category_of_writer = order_params["writerCategory"]

      o.vip_support = ( order_params["vipSupport"].present? ? 1 : 0 )
      o.abstract_page = ( order_params["abstractPage"].present? ? 1 : 0 )
      o.instructions = order_params["instructions"]
      o.total = order_params["orderTotal"]

      o.country = order_params["country"]
      o.state = order_params["state"]
      o.city = order_params["city"]
      o.post_code = order_params["zipCode"]
      o.address = "#{order_params['addrLine1']} #{order_params['addrLine2']}"

      o.auth_token = order_params["token"]
      o.deadline = order_params["deadline"]
    end

    Twocheckout::API.credentials = {
        :seller_id => '1871610',
        :private_key => '4FBACD6F-C8BF-40C1-9365-EF0DC5207932',
        :sandbox => false
    }

    discount = Coupon.where("code = '#{order_params['coupon']}' AND valid_from <= '#{Date.today.to_time}' AND valid_to >= '#{Date.today.to_time}' AND is_active = 1").first if order_params['coupon']

    if discount
      order['total'] = discount['percent_value'] ?
          ( order['total'].to_f * (100 - discount['percent_value']) ).round / 100.0 :
          order['total'].to_f - discount['fixed_value']
    end

    params = {
        :merchantOrderId => SecureRandom.urlsafe_base64(nil, false),
        :token => order['auth_token'],
        :currency => 'USD',
        :total => order['total'], #1.00,
        :billingAddr => {
            :name => order_params['nameOnCard'],
            :addrLine1 => order['address'],
            :city => order['city'],
            :state => order['state'],
            :zipCode => order['post_code'],
            :country => order['country'],
            :email => order['email'],
            :phoneNumber => order['phone']
        }
    }

    error = nil
    begin
      Twocheckout::Checkout.authorize(params)
    rescue Twocheckout::TwocheckoutError => e
      error = e.message
    end

    order.save!

    # if order_params[:document]
    #   upload = Upload.create( { document: order_params[:document], order: order } )
    #   render text: @upload.errors.full_messages.join(','), status: 400 unless upload.save
    # end
    if order_params[:upload_id].present? && !order_params[:upload_id].blank?
      upload = Upload.find_by(id: order_params[:upload_id])
      upload.update(order: order) if upload
    end

    if error
      order.update_attribute(:status, false)
      OrderMailer.failed_attempt_email(order).deliver_now!
    else
      order.update_attribute(:status, true)
      OrderMailer.confirmation_email(order).deliver_now!
    end

    render text: error || 'APPROVED', status: 200
  end

  def upload
    if order_params[:document]
      upload = Upload.create( { document: order_params[:document] } )

      begin
        upload.save
      rescue Exception => e
        render text: upload.errors.full_messages.join(','), status: 400
      end
      
      render text: upload.id
    end
  end

  private
  def order_params
    params.permit(:name,
                  :email,
                  :phone,
                  :academicLevel,
                  :typeOfPaper,
                  :paperFormat,
                  :topic,
                  :instructions,
                  :quantity,
                  :pageQuantity,
                  :deadline,
                  :subject,
                  :writerCategory,
                  :slideQuantity,
                  :countryCode,
                  :phone,
                  :abstractPage,
                  :vipSupport,
                  :token,
                  :nameOnCard,
                  :ccNo,
                  :cvc,
                  :country,
                  :coupon,
                  :state,
                  :city,
                  :zipCode,
                  :addrLine1,
                  :addrLine2,
                  :orderTotal,
                  :document,
                  :upload_id)
  end
end
