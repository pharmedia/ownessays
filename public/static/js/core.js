'use strict';

var OwnEssays = {
    init: function () {

        this.Basic.init();
        this.Component.init();
        $('#page-loader').fadeOut(500);

        $('.form-group .input-custom label').click(function () {
            if ($(this).parent().hasClass('additional-service-wrap')) {
                $(this).toggleClass('active');
            } else {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
            }

            var value = $(this).attr('data-value');
            if (value) {
                OwnEssays.Wizard.data.sum.push($(this).attr('data-value'));
            }
        });

        $('a').click(function () {
            var uri = this.href.split('#')[1];
            if (uri) $('html, body').animate({scrollTop: $("#"+uri).offset().top}, 500, 'easeInSine');
        });

        $(':file').change(function(){
            if (!$('.file-choose-mark-error').hasClass('hidden')) $('.file-choose-mark-error').addClass('hidden');
            if (!$('.file-choose-mark').hasClass('hidden')) $('.file-choose-mark').addClass('hidden');
            
            var file = this.files[0];
            OwnEssays.Uploads.file = file;
            OwnEssays.Uploads.name = file.name;
            OwnEssays.Uploads.size = file.size;
            OwnEssays.Uploads.type = file.type;

            var formData = new FormData();
            if ( OwnEssays.Uploads.file && OwnEssays.Uploads.size > 1000 )
                formData.append('document', OwnEssays.Uploads.file, OwnEssays.Uploads.name);

            formData.append('authenticity_token', $('#authenticity_token').val());

            $.ajax({
                type: "POST",
                url: "/upload/",
                data: formData,
                async: false,
                xhr: function() {
                    var myXhr = jQuery.ajaxSettings.xhr();
                    if (myXhr.upload) { myXhr.upload.addEventListener('progress', OwnEssays.Uploads.progressHandlingFunction, false); }
                    return myXhr;
                },
                success: function (msg) {
                    $('#upload_id').val(msg);

                    if (!$('.file-choose-mark-error').hasClass('hidden')) $('.file-choose-mark-error').addClass('hidden');
                    $('.file-choose-mark').removeClass('hidden');
                },
                cache: false,
                contentType: false,
                processData: false,
                timeout: 10000,
                error: function (jqXHR) {
                    alert('The chosen file is not supported. Send your file direclty to support@ownessays.com once order is placed');
                    
                    if (!$('.file-choose-mark').hasClass('hidden')) $('.file-choose-mark').addClass('hidden');
                    $('.file-choose-mark-error').removeClass('hidden');
                }
            });
        });
    },

    Uploads: {
        file: false,
        name: false,
        size: false,
        type: false,

        modalShown: false,

        progressHandlingFunction: function(e) {
            if(e.lengthComputable){
                console.log(e.loaded);
                //jQuery('progress').attr( { value: e.loaded, max: e.total} );
            }
        }
    },

    Wizard: {
        settings: {
            phd_dates: false,
        },

        init: function () {
            $('form#orderFormFirstStep').change(function () {
                OwnEssays.Wizard.process('orderFormFirstStep');
            });

            $('form#orderFormSecondStep').change(function () {
                OwnEssays.Wizard.process('orderFormSecondStep');
            });

            $('form#registrationForm').change(function () {
                OwnEssays.Wizard.process('registrationForm');
            });

            $('#srcQuantity').on('changed.fu.spinbox', function () {
                OwnEssays.Wizard.process('orderFormFirstStep');
            });

            $('#SourceQuantity').on('changed.fu.spinbox', function () {
                OwnEssays.Wizard.process('orderFormSecondStep');
            });

            $('#pageQuantity').on('changed.fu.spinbox', function () {
                OwnEssays.Wizard.process('orderFormSecondStep');
            });

            $('.deadline label').click(function () {
                var input = $(this).prop("for");
                var ts = parseInt($('input#' + input).data('ts')) * 1000;
                var deadline = new Date(Date.now() + ts);

                $('span.date-color').html(deadline.toLocaleString());
            });

            $('label.academic-label').click(function () {
                var label = $(this).attr("for");
                if (label == "academic-level-checkbox-phd" && !OwnEssays.Wizard.settings.phd_dates) {
                    $('div.deadline label').toggleClass('hidden');
                    OwnEssays.Wizard.settings.phd_dates = true;
                } else if (OwnEssays.Wizard.settings.phd_dates) {
                    $('div.deadline label').toggleClass('hidden');
                    OwnEssays.Wizard.settings.phd_dates = false;
                }

                if (label == "academic-level-checkbox-specialized") {
                    $('div.deadline label[for="deadline-9-h"]').hide();
                } else if (label != "academic-level-checkbox-phd") {
                    $('div.deadline label[for="deadline-9-h"]').show();
                }
            });

            $('.back-button-wrapper').click(function () {
                OwnEssays.Wizard.prev();
            })

        },

        forms: {
            orderFormFirstStep: {
                academicLevel: "High School",
                typeOfPaper: "Essay (any type)",
                subject: "",
                topic: "",
                instructions: "",
                quantity: "",
                paperFormat: "MLA",
                file: "",
                upload_id: "",
            },

            orderFormSecondStep: {
                pageQuantity: 1,
                deadline: "5 days",
                writerCategory: "Most suitable",
                slideQuantity: 0,
                abstractPage: undefined,
                vipSupport: undefined,
            },

            registrationForm: {
                email: "",
                name: "",
                countryCode: "",
                phone: "",
                coupon: "",
            },

            checkoutForm: {
                token: "",
                ccNo: "",
                expiry: "",
                cvc: "",
                nameOnCard: "",

                country: "",
                state: "",
                city: "",
                zipCode: "",
                addrLine1: "",
                addrLine2: ""
            },

            validators: {
                orderFormFirstStep: function () {
                    var form = $('form#orderFormFirstStep');
                    form.validate({
                        errorElement: 'span',
                        errorContainer: '.form-error',
                        errorLabelContainer: ".form-error ul",
                        wrapper: "li",
                        rules: {
                            academicLevel: {required: true},
                            typeOfPaper: {required: true},
                            subject: {required: true, min: 0},
                            topic: {
                                required: true,
                                minlength: 2
                            },
                            paperFormat: {required: true},
                        },
                        messages: {
                            academicLevel: {
                                required: "Please choose academic level."
                            },
                            typeOfPaper: {
                                required: "Please choose type of paper."
                            },
                            subject: {
                                required: "Please choose subject."
                            },
                            topic: {
                                required: "Please provide paper title."
                            },
                            paperFormat: {
                                required: "Please choose paper format."
                            },
                        }
                    });

                    return form.valid();
                },

                orderFormSecondStep: function () {
                    var form = $('form#orderFormSecondStep');
                    form.validate({
                        errorElement: 'span',
                        errorContainer: '.form-error',
                        errorLabelContainer: ".form-error ul",
                        wrapper: "li",
                        rules: {
                            pageQuantity: {required: true, min: 1},
                            deadline: {required: true},
                            writerCategory: {required: true},
                        },
                        messages: {
                            pageQuantity: {
                                min: "Please choose at last one page.",
                            },
                            deadline: {
                                required: "Please choose final deadline."
                            },
                            writerCategory: {
                                required: "Please choose Writer category."
                            }
                        }
                    });

                    return form.valid();
                },

                registrationForm: function () {
                    var form = $('form#registrationForm');
                    form.validate({
                        errorElement: 'span',
                        errorContainer: '.form-error',
                        errorLabelContainer: ".form-error ul",
                        wrapper: "li",
                        rules: {
                            name: {
                                required: true,
                                minlength: 2
                            },
                            email: {
                                required: true,
                                email: true
                            },
                            email_confirm: {
                                equalTo: '#order_email'
                            },
                            phone: {
                                required: true,
                                // digits: true,
                                // minlength: 9
                            }
                        },
                        messages: {
                            name: {
                                required: "Please enter your name.",
                                minlength: "Your name needs to be at least 2 characters"
                            },
                            email: {
                                required: "Please enter your email address.",
                                minlength: "You entered an invalid email address."
                            },
                            email_confirm: {
                                required: "Please enter your email address again.",
                                equalTo: "Emails you're entered didn't match."
                            },
                            phone: {
                                required: "Please enter your phone.",
                                minlength: "Your phone needs to be at least 9 digits"
                            }
                        }
                    });

                    return form.valid();
                },

                checkoutForm: function () {
                    var form = $('form#checkoutForm');
                    form.validate({
                        errorElement: 'span',
                        errorContainer: '.form-error',
                        errorLabelContainer: ".form-error ul",
                        wrapper: "li",
                        rules: {
                            ccNo: {
                                required: true
                            },
                            expiry: {
                                required: true
                            },
                            nameOnCard: {
                                required: true
                            },
                            cvc: {
                                required: true,
                                digits: true
                            }
                        },
                        messages: {
                            ccNo: {
                                required: "Please enter valid credit card number"
                            },
                            expiry: {
                                required: "Please provide expiration data (mm/yyyy)"
                            },
                            nameOnCard: {
                                required: "Please specify name on card"
                            },
                            cvc: {
                                required: "Please provide CVC/CVC code (3 or 4 digits)"
                            },

                            country: {
                                required: "Please specify your country"
                            },

                            state: {
                                required: "Please specify your state or county"
                            },

                            city: {
                                required: "Please specify your city"
                            },

                            zipCode: {
                                required: "Please specify your ZIP (postal) code"
                            },

                            addrLine1: {
                                required: "Please specify your street address"
                            }
                        }
                    });

                    return form.valid();
                },
            }
        },

        matrix: {
            "High School": {
                "5 days": 13.99,
                "4 days": 14.99,
                "3 days": 15.99,
                "48 hours": 18.99,
                "24 hours": 22.99,
                "12 hours": 25.99,
                "9 hours": 28.99
            },

            "Undergraduate": {
                "5 days": 14.99,
                "4 days": 15.99,
                "3 days": 17.99,
                "48 hours": 20.99,
                "24 hours": 23.99,
                "12 hours": 27.99,
                "9 hours": 30.99
            },

            "Masters": {
                "5 days": 15.99,
                "4 days": 17.99,
                "3 days": 19.99,
                "48 hours": 21.99,
                "24 hours": 24.99,
                "12 hours": 28.99,
                "9 hours": 33.99
            },

            "PhD": {
                "45 days": 25.99,
                "30 days": 27.99,
                "21 days": 29.99,
                "14 days": 35.99,
                "7 days": 39.99
            },

            "Econ/Math": {
                "5 days": 20.99,
                "4 days": 23.99,
                "3 days": 27.99,
                "48 hours": 31.99,
                "24 hours": 34.99,
                "12 hours": 39.99
            },

            "Abstract": 11.99,
            "VIP Support": 14.99,
            "Advanced": 4.0,
            "Preferred": 4.0,
            "PowerPoint": 6.0
        },

        calculate: function () {
            var s1 = this.forms.orderFormFirstStep;
            var s2 = this.forms.orderFormSecondStep;
            if (s1.academicLevel == undefined || s2.pageQuantity == undefined || s2.deadline == undefined) {
                return;
            }

            var mtx = this.matrix;
            var level = mtx[s1.academicLevel];
            var cost = level[s2.deadline];

            switch (s1.typeOfPaper) {
                case 'Admission essay':
                case 'Business plan':
                case 'Capstone project':
                case 'Case study':
                case 'Research paper':
                case 'Research proposal':
                    cost = cost * 1.25;
                    break;
                case 'Presentation':
                    cost = cost * 0.67;
                    break;
                default:
                    break;
            }

            if (s2.writerCategory == "Advanced") cost += mtx["Advanced"];
            if (s2.writerCategory == "Preferred") cost += mtx["Preferred"];

            var sum = cost * parseInt(s2.pageQuantity);

            sum += parseInt(s2.slideQuantity) * mtx["PowerPoint"];
            if (s2.abstractPage != undefined) sum += mtx["Abstract"];
            if (s2.vipSupport != undefined) sum += mtx["VIP Support"];

            OwnEssays.Wizard.orderTotal = Math.round(sum * 100) / 100;

            $('.total-sum-value').html(OwnEssays.Wizard.orderTotal);

            Object.keys(s1).forEach(function (e, i, arr) {
                var el = $('span[data-type="' + e + '"]');
                el.html(s1[e]);
            });

            Object.keys(s2).forEach(function (e, i, arr) {
                var el = $('span[data-type="' + e + '"]');
                el.html(s2[e]);
            });

            var addServicesStr = "";
            if (s2.vipSupport != undefined) addServicesStr += s2.vipSupport + " ";
            if (s2.abstractPage != undefined) addServicesStr += s2.abstractPage;
            if (addServicesStr.length == 0) addServicesStr = "None";
            $('span[data-type="addServices"]').html(addServicesStr);
        },

        submit: function () {
            var response = "";
            var data = $.extend(OwnEssays.Wizard.forms.orderFormFirstStep,
                OwnEssays.Wizard.forms.orderFormSecondStep,
                OwnEssays.Wizard.forms.registrationForm,
                OwnEssays.Wizard.forms.checkoutForm,
                {authenticity_token: $('#authenticity_token').val(), orderTotal: OwnEssays.Wizard.orderTotal});

            var formData = new FormData();
            $.each(data, function (key, value) {
                formData.append(key, value);
            });
            //if ( OwnEssays.Uploads.file && OwnEssays.Uploads.size > 1000 )
            //    formData.append('document', OwnEssays.Uploads.file, OwnEssays.Uploads.name);

            _gaq.push(['_trackEvent', 'Order Form', 'Submit to Payment Processor', 'Order Flow']);

            $.ajax({
                type: "POST",
                url: "/order/",
                data: formData,
                async: false,
                xhr: function() {
                    var myXhr = jQuery.ajaxSettings.xhr();
                    if (myXhr.upload) { myXhr.upload.addEventListener('progress', OwnEssays.Uploads.progressHandlingFunction, false); }
                    return myXhr;
                },
                success: function (msg) {

                    $.unblockUI();

                    if (msg == 'APPROVED') {
                        window.top.location = 'http://ownessays.com/order/';
                    }
                    else {
                        $("#orderNow").show();
                        alert("There is an issue with your order. Please check your credit card data or try again with another card.");
                    }
                },
                cache: false,
                contentType: false,
                processData: false,
                timeout: 10000,
                error: function (jqXHR) {
                    $.unblockUI();

                    alert('Server error. Please retry.');
                }
            });
        },

        process: function (step) {
            var form = this.forms[step];
            var fields = Object.keys(form);

            if (!fields.length) return;

            fields.forEach(function (field, idx, arr) {

                var input = $("form#" + step + " [name='" + field + "']");
                var value = undefined;
                switch ($(input).attr("type")) {
                    case "text":
                    case "textarea":
                        if ($(input).hasClass('spinbox-input')) {
                            value = $(input).parent().spinbox('value');
                        } else {
                            value = $(input).val();
                            if (( value == undefined || value.length < 2) && !$(input).prop('required')) {
                                value = $(input).attr("placeholder");
                            }
                        }
                        break;
                    case "checkbox":
                        value = $(input).prop('checked') ? $(input).val() : undefined;
                        break;
                    case "radio":
                        value = $(input).filter(':checked').val();
                        break;
                    case "file":
                        value = $(input).val();
                        break;
                    default:
                        value = $(input).children(":selected").attr("label");
                        if (value == undefined) value = $(input).val();
                        break;
                }

                form[field] = value;

                OwnEssays.Wizard.calculate();
            });
        },

        orderTotal: "",

        next: function (step) {
            this.process(step);

            _gaq.push(['_trackEvent', 'Order Form', 'Step: '+step, 'Order Flow']);

            if (OwnEssays.Wizard.forms.validators[step]()) {
                if (step != 'checkoutForm') $('#orderWizard').wizard('next');
            }

            $('#orderNow').animate({ scrollTop: 0 }, 'slow');
        },

        prev: function () {
            $('#orderWizard').wizard('previous');
        }

    },

    Basic: {
        init: function () {

            this.animations();
            this.backgrounds();
            this.scroller();
            this.masonry();
            this.ajaxLoader();
            this.mobileNav();
            this.verticalCenter();
            this.map();
            this.forms();


            OwnEssays.Wizard.init();

            $('[data-toggle="popover"]').popover();
        },
        backgrounds: function () {

            // Images
            $('.bg-image').each(function () {
                var src = $(this).children('img').attr('src');
                $(this).css('background-image', 'url(' + src + ')').children('img').hide();
            });

        },
        animations: function () {
            // Animation - hover
            $('.animated-hover')
                .on('mouseenter', function () {
                    var animation = $(this).data('hover-animation');
                    var duration = $(this).data('hover-animation-duration');
                    $(this).stop().css({
                        '-webkit-animation-duration': duration + 'ms',
                        'animation-duration': duration + 'ms'
                    }).addClass(animation);
                })
                .on('mouseleave', function () {
                    var $self = $(this);
                    var animation = $(this).data('hover-animation');
                    var duration = $(this).data('hover-animation-duration');
                    $(this).stop().removeAttr('style').removeClass(animation);
                });

            // Animation - appear
            $('.animated').appear(function () {
                $(this).each(function () {
                    var $target = $(this);
                    var delay = $(this).data('animation-delay');
                    setTimeout(function () {
                        $target.addClass($target.data('animation')).addClass('visible')
                    }, delay);
                });
            });
        },
        scroller: function () {

            var $header = $('#header');
            var $mobileNav = $('#mobile-nav');
            var $section = $('.section', '#page-wrapper');
            var $body = $('body');

            var scrollOffset;
            if ($header.hasClass('header-horizontal')) scrollOffset = -65;
            else scrollOffset = 0;

            $('#header, #mobile-nav, [data-target="local-scroll"]').localScroll({
                offset: scrollOffset,
                duration: 800,
                easing: $('#page-wrapper').data('scroll-easing')
            });

            $('#page-wrapper.padding-t-header').css('padding-top', $header.outerHeight() + 'px');

            // Scroll Progress

            var $scrollProgress = $('#scroll-progress');

            $(window).scroll(function () {
                var scrolled = $(window).scrollTop();
                var windowWidth = $(window).width();
                var windowHeight = $(window).height()
                var bodyHeight = $('body').height() - windowHeight;
                var height = ((scrolled / bodyHeight) * windowWidth);
                setTimeout(function () {
                    $scrollProgress.css('width', height + 'px');
                }, 100);
            });

            var changeNavBg = function ($section) {
                if ($section.data('header-change') == true) {
                    var color = $section.css('background-color');
                    $header.css('background-color', color);
                    $mobileNav.css('background-color', color);

                    if (color == 'rgb(255, 255, 255)' || color == 'rgb(248, 248, 248)') {
                        $header.css('background-color', 'rgba(0,0,0,0.6)');
                        $mobileNav.css('background-color', 'rgba(0,0,0,0.6)');
                    }

                } else if ($section.data('header-transparent') == true) {
                    $header.css('background-color', 'transparent');
                } else {
                    $header.css('background-color', '');
                    $mobileNav.css('background-color', '');
                }
            };

            var $menuItem = $('#main-menu li > a, #side-nav li > a');
            var checkMenuItem = function (id) {
                $menuItem.each(function () {
                    var link = $(this).attr('href');
                    if (id == link) $(this).addClass('active');
                    else $(this).removeClass('active');
                });
            };

            $body.waypoint({
                handler: function (direction) {
                    $header.toggleClass('sticky')
                },
                offset: function () {
                    return -$(window).height() + 66;
                }
            });
            $body.waypoint({
                handler: function (direction) {
                    $header.toggleClass('unvisible')
                },
                offset: function () {
                    return -$header.height();
                }
            });
            $section.waypoint({
                handler: function (direction) {
                    if (direction == 'up') {
                        var $activeSection = $(this.element);
                        var id = '#' + this.element.id;
                        checkMenuItem(id);
                        changeNavBg($activeSection);
                    }
                },
                offset: function () {
                    if ($header.hasClass('header-horizontal')) return -this.element.clientHeight + 66;
                    else return -$(window).height() / 2;
                }
            });
            $section.waypoint({
                handler: function (direction) {
                    if (direction == 'down') {
                        var $activeSection = $(this.element);
                        var id = '#' + this.element.id;
                        checkMenuItem(id);
                        changeNavBg($activeSection);
                    }
                },
                offset: function () {
                    if ($header.hasClass('header-horizontal')) return 66;
                    else return $(window).height() / 2;
                }
            });
            $(window).resize(function () {
                setTimeout(function () {
                    Waypoint.refreshAll()
                }, 600);
            });
        },
        masonry: function () {

            var $grid = $('.masonry-grid', '#page-wrapper');

            $grid.masonry({
                columnWidth: '.masonry-sizer',
                itemSelector: '.masonry-item',
                percentPosition: true
            });

            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });

            $grid.on('layoutComplete', Waypoint.refreshAll());

        },
        ajaxLoader: function () {

            var toLoad;
            var offsetTop;

            var $ajaxLoader = $('#ajax-loader');
            var $ajaxModal = $('#ajax-modal');
            var isAjaxModal = false;

            function showNewContent() {
                $ajaxModal.fadeIn(200, function () {
                    $('html').addClass('locked-scrolling');
                });
            }

            function loadContent() {
                $ajaxModal.load(toLoad);
            }

            $('a[data-target="ajax-modal"]').on('click', function () {
                isAjaxModal = true;
                offsetTop = $(document).scrollTop();
                toLoad = $(this).attr('href');
                loadContent();
                return false;
            });

            $(document).ajaxStart(function () {
                if (isAjaxModal) $ajaxLoader.fadeIn(200);
            });
            $(document).ajaxStop(function () {
                if (isAjaxModal) {
                    $ajaxLoader.fadeOut(200, function () {
                        showNewContent();
                    });
                }
            });

            function closeDetails() {
                isAjaxModal = false;
                $('html').removeClass('locked-scrolling');
                $(document).scrollTop(offsetTop)
                $ajaxModal.fadeOut(200);
            }

            $ajaxModal.delegate('*[data-dismiss="close"]', 'click', function () {
                closeDetails();
                return false;
            });

        },
        mobileNav: function () {
            $('[data-target="mobile-nav"]').on('click', function () {
                $('body').toggleClass('mobile-nav-open');
                return false;
            });
        },
        verticalCenter: function () {

            var vCenter = function () {
                $('.v-center').each(function () {
                    $(this).css({
                        'margin-top': ($(this).parent().height() / 2) - ($(this).outerHeight() / 2) + 'px'
                    })
                });
            };
            vCenter();
            $(window).resize(vCenter);

        },
        map: function () {

            function mapInitialize() {

                var $googleMap = $('#google-map');

                var yourLatitude = $googleMap.data('latitude');
                var yourLongitude = $googleMap.data('longitude');

                var pickedStyle = $googleMap.data('style');
                var myOptions = {
                    zoom: 14,
                    center: new google.maps.LatLng(yourLatitude, yourLongitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: false,
                    panControl: false,
                    zoomControl: true,
                    scaleControl: false,
                    streetViewControl: false,
                    scrollwheel: false
                };

                window.map = new google.maps.Map(document.getElementById('google-map'), myOptions);

                var image = '/static/img/my-location.png';
                var myLatLng = new google.maps.LatLng(yourLatitude, yourLongitude);
                var myLocation = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    icon: image
                });

            }

            google.maps.event.addDomListener(window, 'load', mapInitialize);

        },

        forms: function () {

            var $formAlert = $('#form-alert');
            var $formError = $('#form-error');


            // Basic Form

            var $basicForm = $('.basic-form');
            $basicForm.validate({
                errorPlacement: function (error, element) {
                }
            });
            $basicForm.submit(function () {
                if (!$basicForm.valid()) $formError.show();
            });

            // Contact Form

            var $contactForm = $('#contact-form');

            $contactForm.validate({
                errorElement: 'span',
                errorContainer: '#form-error',
                errorLabelContainer: "#form-error ul",
                wrapper: "li",
                rules: {
                    name: {
                        required: true,
                        minlength: 2
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 10
                    }
                },
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: "Your name needs to be at least 2 characters"
                    },
                    email: {
                        required: "Please enter your email address.",
                        minlength: "You entered an invalid email address."
                    },
                    message: {
                        required: "Please enter a message.",
                        minlength: "Your message needs to be at least 10 characters"
                    }
                }
            });

            $contactForm.submit(function () {
                var response;
                $formAlert.hide().text();
                if ($contactForm.valid()) {
                    $.ajax({
                        type: "POST",
                        url: "/contact/",
                        data: $(this).serialize(),
                        success: function (msg) {
                            if (msg === 'SEND') {
                                response = '<div class="alert alert-success">Thank you for your inquiry. Your question is important to us. One of our administrators will review your query and respond to you within the next 12-24 hours.</div>';
                            }
                            else {
                                response = '<div class="alert alert-danger">Ooops... It seems that we have a problem.</div>';
                            }
                            $formAlert.prepend(response);
                            $formAlert.show();
                        }
                    });
                    return false;
                }
                return false;
            });

        }
    },
    Component: {
        init: function () {

            this.carousel();
            this.modal();
            this.chart();
            this.tooltip();
            this.popover();

        },
        modal: function () {

            $('.modal').on('show.bs.modal', function () {
                $('body').addClass('modal-opened');
            });

            $('.modal').on('hide.bs.modal', function () {
                $('body').removeClass('modal-opened');
            });

            $('#mapModal').on('shown.bs.modal', function () {
                google.maps.event.trigger(map, 'resize');
            });

        },
        chart: function () {

            $('.chart').each(function () {

                var size = $(this).data('size');

                $(this)
                    .easyPieChart({
                        barColor: $(this).data('bar-color'),
                        trackColor: $(this).data('track-color'),
                        scaleColor: false,
                        size: size,
                        lineWidth: $(this).data('line-width'),
                        animate: 1000,
                        onStep: function (from, to, percent) {
                            $(this.el).find('.percent').text(Math.round(percent));
                        },
                        onStart: function () {
                            OwnEssays.Basic.verticalCenter();
                        }
                    })
                    .css({
                        'width': size + 'px',
                        'height': size + 'px'
                    })
                    .children('.percent').css('line-height', size + 'px');

            });

            $('.chart').appear(function () {
                $(this).each(function () {
                    var $chart = $(this);
                    var value = $(this).data('value');
                    setTimeout(function () {
                        $chart.data('easyPieChart').update(value);
                    }, 200);
                });
            });
        },
        checkBrowser: function () {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
                if (ua.indexOf('chrome') > -1) {
                    $('html').addClass('chrome');
                    window.chrome = true;
                } else {
                    $('html').addClass('safari');
                    window.safari = true;
                }
            }
            if (ua.indexOf('msie') > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                $('html').addClass('ie');
                window.msie = true;
            }
        },
        onResize: function (func) {
            $(window).resize(function () {
                setTimeout(func(), 400);
            });
        },
        editableAlpha: function () {

            $('.overlay, .editable-alpha').each(function () {
                $(this).css('opacity', $(this).attr('data-alpha') / 100);
            });

        },
        carousel: function () {
            $('.carousel').owlCarousel({
                items: $(this).data('items'),
                itemsDesktop: $(this).data('items-desktop'),
                itemsDesktopSmall: false,
                itemsTablet: $(this).data('items-tablet'),
                itemsMobile: $(this).data('items-mobile'),
                singleItem: $(this).data('single-item'),
                autoPlay: $(this).data('auto-play'),
                pagination: $(this).data('pagination'),
                stopOnHover: true,
                afterInit: function () {
                    OwnEssays.Basic.verticalCenter();
                }
            });
        },
        tooltip: function () {
            $("[data-toggle='tooltip']").tooltip();
        },
        popover: function () {
            $("[rel='popover']").popover();
        }
    },

    TCO: {
        successCallback: function (data) {
            OwnEssays.Wizard.forms.checkoutForm.token = data.response.token.token;

            OwnEssays.Wizard.submit();

            return false;
        },

        errorCallback: function (data) {
            if (data.errorCode === 200) {
                // Should retry with request here.
                alert("Processing server fault. Please, retry.");
            } else {
                alert('Please check your credit card data and try again.');
            }

            $.unblockUI();

            $("#orderNow").show();
        },

        tokenRequest: function () {
            var args = {
                sellerId: "1871610",
                publishableKey: "62D8B1DD-EF6F-4F59-942B-4CB9DC09D9FA",
                ccNo: $("#ccNo").val(),
                cvv: $("#cvc").val(),
                expMonth: parseInt($("#expiry").val().substr(0, 2)),
                expYear: parseInt($("#expiry").val().substr(-2))
            };

            $("#orderNow").hide();

            $.blockUI({ css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }, message: '<h2>Processing...</h2>'});

            TCO.requestToken(OwnEssays.TCO.successCallback, OwnEssays.TCO.errorCallback, args);
        }

    }
};

function iDunnoButItShoudFixThatWeirdMobileChromeBug() {
    jQuery('body span').last().remove();
}

$(document).ready(function () {
    if (jQuery("#checkoutForm").size() > 0)
        var card = new Card({
            form: '#checkoutForm',
            container: '.card-wrapper',

            formSelectors: {
                numberInput: 'input#ccNo', // optional — default input[name="number"]
                expiryInput: 'input#expiry', // optional — default input[name="expiry"]
                cvcInput: 'input#cvc', // optional — default input[name="cvc"]
                nameInput: 'input#nameOnCard' // optional - defaults input[name="name"]
            },

            width: 200,
            formatting: true,

            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },

            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Name on Card',
                expiry: '••/••',
                cvc: '•••'
            },

            debug: false
        });

    OwnEssays.init();

    TCO.loadPubKey('production');

    setTimeout(iDunnoButItShoudFixThatWeirdMobileChromeBug, 1000);


    var timeoutID; resetTimeout();
    function resetTimeout(){
        if( timeoutID ) clearTimeout( timeoutID );
        timeoutID = setTimeout( ShowTimeoutWarning, 600000 );
    }
    function ShowTimeoutWarning() {
        var confReload = confirm('Session has been expired. To continue with your order page should be reloaded. Reload page?');
        if (confReload) location.reload();
        return false;
    }

    document.onkeyup   = resetTimeout;
    document.onkeydown = resetTimeout;
    document.onclick   = resetTimeout;
    
});
