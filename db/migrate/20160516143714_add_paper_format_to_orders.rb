class AddPaperFormatToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :paper_format, :string
  end
end
