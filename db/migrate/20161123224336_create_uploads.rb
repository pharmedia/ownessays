class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.references :order, index: true

      t.timestamps null: false
    end
  end
end
