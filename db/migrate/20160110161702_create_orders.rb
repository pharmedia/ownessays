class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :auth_token
      t.string :status

      t.string :academic_level
      t.string :type_of_paper
      t.string :subject
      t.string :topic
      t.integer :sources
      t.integer :pages
      t.string :category_of_writer
      t.integer :powerpoint_slides
      t.text :additional_services
      t.string :deadline


      t.timestamps null: false
    end
  end
end
