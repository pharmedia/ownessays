class AddPageIdToBlocks < ActiveRecord::Migration
  def change
    add_column :blocks, :page_id, :integer
    add_index :blocks, :page_id
  end
end
