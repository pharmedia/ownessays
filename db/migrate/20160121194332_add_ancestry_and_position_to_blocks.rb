class AddAncestryAndPositionToBlocks < ActiveRecord::Migration
  def change
    add_column :blocks, :ancestry, :string
    add_column :blocks, :position, :integer
  end
end
