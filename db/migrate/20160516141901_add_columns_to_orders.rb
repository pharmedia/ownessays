class AddColumnsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :vip_support, :boolean
    add_column :orders, :abstract_page, :boolean
    add_column :orders, :instructions, :text
    add_column :orders, :total, :float

    remove_column :orders, :additional_services
  end
end
