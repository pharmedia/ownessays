class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :subtitle
      t.text :content
      t.string :base_color
      t.string :ancestry
      t.integer :position

      t.timestamps null: false
    end

    add_index :pages, :ancestry
    add_index :pages, :position
  end
end
