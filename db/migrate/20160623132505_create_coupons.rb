class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :code, index: true
      t.datetime :valid_from
      t.datetime :valid_to
      t.boolean :is_active, default: true
      t.integer :percent_value
      t.integer :fixed_value

      t.timestamps null: false
    end
  end
end
